package mas.iva.config;

import java.util.logging.Level;

public class Properties {

    public final static Level LOGGING_LEVEL = Level.OFF;

    public final static String PROJECT_PATH = "C:/Users/iulia/Downloads/ModellingPuzzlesInFirstOrderLogic_GrozaAdrian/ModellingPuzzlesInFirstOrderLogic_GrozaAdrian2";
    public final static String MAIN_TEX_PATH = PROJECT_PATH + "/main.tex";
    public final static String TEX_EXTENSION = ".tex";

}
