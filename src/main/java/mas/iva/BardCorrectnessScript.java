package mas.iva;

import mas.iva.model.AnnotatedString;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class BardCorrectnessScript {

    public static final String BARD_SOLUTIONS_LOCATION = "C:/Users/iulia/Downloads/ModellingPuzzlesInFirstOrderLogic_GrozaAdrian/Puzzles/GoogleBardSolutions";
    public static final String BARD_SOLUTION_START_TAG = "solbard{";
    public static final String BARD_SOLUTION_END_TAG = "}";

    public static void main(String[] args) {
        double totalPuzzles = 100.0;
        Map<String, Integer> verdictCounts = new HashMap<>();

        for (int i = 1; i <= totalPuzzles; i++) {
            String filePath = BARD_SOLUTIONS_LOCATION + "/puzzle " + i + ".txt";
            try {
                BufferedReader reader = new BufferedReader(new FileReader(filePath));
                String line;
                String verdict = "";

                while ((line = reader.readLine()) != null && verdict.isBlank()) {
                    if (line.startsWith(BARD_SOLUTION_START_TAG)) {
                        verdict = AnnotatedString.findNext(line, BARD_SOLUTION_START_TAG, BARD_SOLUTION_END_TAG).getContent();
                        System.out.println("puzzle " + i + ", " + verdict);
                        if (verdictCounts.containsKey(verdict)) {
                            verdictCounts.put(verdict, verdictCounts.get(verdict) + 1);
                        } else {
                            verdictCounts.put(verdict, 1);
                        }
                    }
                }
                reader.close();
            } catch (IOException e) {
                e.printStackTrace();
            }

        }
        verdictCounts.keySet().forEach(v -> System.out.println(v + ", " + verdictCounts.get(v) / totalPuzzles * 100 + "%"));
    }

}
