package mas.iva.model;

public class Fault {

    public String faultType;
    public String description;
    public String raw;

    public static Fault extractFault(String raw) {
        Fault f = new Fault();
        f.raw = raw;
        int index = raw.indexOf(":");
        if (index == -1) {
            f.faultType = f.raw.toLowerCase().replace(',', ' ');
            f.description = "";
        } else {
            f.faultType = f.raw.substring(0, index).toLowerCase();
            f.description = f.raw.substring(index + 1);
        }
        return f;
    }
}
