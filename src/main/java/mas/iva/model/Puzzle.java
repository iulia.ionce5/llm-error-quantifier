package mas.iva.model;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static mas.iva.Main.getStringEnclosedBy;
import static mas.iva.config.Properties.PROJECT_PATH;
import static mas.iva.config.Properties.TEX_EXTENSION;

public class Puzzle {

    public String name;
    public String locativeName;
    public String rawPuzzle;
    public List<Solution> solutions;
    public Chapter chapter;

    public String getPath() {
        if (locativeName == null || locativeName.isBlank()) {
            throw new RuntimeException("Cannot determine puzzle path because locative name is " + locativeName);
        }
        return PROJECT_PATH + "/" + locativeName + TEX_EXTENSION;
    }

    public static Puzzle constructPuzzle(String locativeName) {
        Puzzle p = new Puzzle();
        p.locativeName = locativeName;

        try {
            BufferedReader reader = new BufferedReader(new FileReader(p.getPath()));
            StringBuilder puzzleBuilder = new StringBuilder();

            // puzzle name is on the first line
            String line = reader.readLine();
            String n1 = getStringEnclosedBy(line, "\\puzzlebox{", "}");
            String n2 = getStringEnclosedBy(line, "\\puzzlebox{", "\\");
            p.name = n1.length() < n2.length() ? n1 : n2;

            while ((line = reader.readLine()) != null) {
                puzzleBuilder.append(line);
            }
            p.rawPuzzle = puzzleBuilder.toString();

            addSolutions(p);

            reader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return p;
    }

    private static void addSolutions(Puzzle p) {
        p.solutions = new ArrayList<>();

        ChatGptSolution gptSol = ChatGptSolution.extractSolution(p);
        p.solutions.add(gptSol);
    }

}
