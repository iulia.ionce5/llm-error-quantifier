package mas.iva.model;

public enum SolutionAuthor {
    CHAT_GPT, GOOGLE_BARD
}
