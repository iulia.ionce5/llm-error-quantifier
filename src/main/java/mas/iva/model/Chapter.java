package mas.iva.model;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static mas.iva.Main.getStringEnclosedBy;
import static mas.iva.config.Properties.PROJECT_PATH;
import static mas.iva.config.Properties.TEX_EXTENSION;

public class Chapter {

    public final static String INCLUDE_PUZZLE_START_DELIMITER = "\\input{";
    public final static String INCLUDE_PUZZLE_END_DELIMITER = "}";

    public String name;
    public String locativeName;
    public List<Puzzle> puzzles;

    public String getPath() {
        if (locativeName == null || locativeName.isBlank()) {
            throw new RuntimeException("Cannot determine chapter path because locative name is " + locativeName);
        }
        return PROJECT_PATH + "/" + locativeName + TEX_EXTENSION;
    }

    public static Chapter constructChapter(String locativeName) {
        Chapter c = new Chapter();
        c.locativeName = locativeName;

        try {
            BufferedReader reader = new BufferedReader(new FileReader(c.getPath()));

            // chapter name is on the first line
            String line = reader.readLine();
            c.name = getStringEnclosedBy(line, "\\chapter{", "}");

            c.puzzles = new ArrayList<>();
            while ((line = reader.readLine()) != null) {
                if (line.startsWith(INCLUDE_PUZZLE_START_DELIMITER)) {
                    String puzzleLocativeName = getStringEnclosedBy(line, INCLUDE_PUZZLE_START_DELIMITER, INCLUDE_PUZZLE_END_DELIMITER);
                    Puzzle p = Puzzle.constructPuzzle(puzzleLocativeName);
                    p.chapter = c;
                    c.puzzles.add(p);
                }
            }

            reader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return c;
    }

}
