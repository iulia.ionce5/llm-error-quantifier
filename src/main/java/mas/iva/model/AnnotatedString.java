package mas.iva.model;

public class AnnotatedString {

    private final Integer si;
    private final Integer ei;
    private final String c;

    public AnnotatedString(Integer si, Integer ei, String c) {
        this.si = si;
        this.ei = ei;
        this.c = c;
    }

    public Integer getStartIndex() {
        return si;
    }

    public Integer getEndIndex() {
        return ei;
    }

    public String getContent() {
        return c;
    }

    /**
     * @param raw   raw text.
     * @param start opening tag
     * @param end   closing tag
     * @return an AnnotatedString object containing the
     * start index of the opening tag,
     * start index of closing tag
     * content between opening and closing tag
     * or null if no such opening tag exists
     */
    public static AnnotatedString findNext(StringBuilder raw, String start, String end) {
        int startI = raw.indexOf(start);
        int endI = raw.indexOf(end, startI);
        if (startI != -1) {
            return new AnnotatedString(startI, endI, raw.substring(startI + start.length(), endI));
        } else {
            return null;
        }
    }

    /**
     * @param raw   raw text.
     * @param start opening tag
     * @param end   closing tag
     * @return an AnnotatedString object containing the
     * start index of the opening tag,
     * start index of closing tag
     * content between opening and closing tag
     * or null if no such opening tag exists
     */
    public static AnnotatedString findNext(String raw, String start, String end) {
        int startI = raw.indexOf(start);
        int endI = raw.indexOf(end, startI);
        if (startI != -1) {
            return new AnnotatedString(startI, endI, raw.substring(startI + start.length(), endI));
        } else {
            return null;
        }
    }
}
