package mas.iva.model;

import java.util.List;

public interface Solution {

    SolutionAuthor getAuthor();

    String getDelimiter();

    String getVerdict();

    List<Fault> getFaults();

    List<String> getStrikes();

    String getGeneratedSolution();

    Double getStrokePercentage();
}
