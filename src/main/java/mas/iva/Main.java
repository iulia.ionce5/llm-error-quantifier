package mas.iva;

import mas.iva.model.Chapter;
import mas.iva.model.Fault;
import mas.iva.model.Puzzle;
import mas.iva.model.Solution;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import static mas.iva.config.Properties.LOGGING_LEVEL;
import static mas.iva.config.Properties.MAIN_TEX_PATH;

public class Main {

    private static final Logger LOGGER = Logger.getLogger("Main");

    public final static String INCLUDE_CHAPTER_START_DELIMITER = "\\include{";
    public final static String INCLUDE_CHAPTER_END_DELIMITER = "}";


    public static void main(String[] args) {
        LOGGER.setLevel(LOGGING_LEVEL);

        int totalFaults = 0;
        Map<String, Integer> faultsCount = new HashMap<>();

        List<Chapter> chapters = readChapters();
        for (Chapter c : chapters) {
            System.out.println(c.name);
            for (Puzzle p : c.puzzles) {
                System.out.println(" ," + p.name);
                for (Solution s : p.solutions) {
                    System.out.println(" , ," + s.getAuthor() + ", " + s.getVerdict().replace(',', ' ') + ", " + s.getStrokePercentage() * 100 + "%");
                    for (Fault f : s.getFaults()) {
                        totalFaults++;
                        Integer partialCount = faultsCount.get(f.faultType);
                        if (partialCount == null) {
                            faultsCount.put(f.faultType, 1);
                        } else {
                            faultsCount.put(f.faultType, partialCount + 1);
                        }
                    }
                }
            }
        }
        System.out.println("Chat GPT mistakes statistics");
        for(String fault : faultsCount.keySet()) {
            double percentage = faultsCount.get(fault) / (double) totalFaults * 100;
            System.out.println(fault + ", " + percentage + "%" );
        }
    }

    /**
     * Assumes all chapters are included at the beginning of the line, enclosed by INCLUDE_CHAPTER_START_DELIMITER and INCLUDE_CHAPTER_END_DELIMITER
     */
    public static List<Chapter> readChapters() {
        List<Chapter> chapters = new ArrayList<>();

        try {
            BufferedReader reader = new BufferedReader(new FileReader(MAIN_TEX_PATH));
            String line;


            while ((line = reader.readLine()) != null) {
                if (line.startsWith(INCLUDE_CHAPTER_START_DELIMITER)) {
                    String chLocativeName = getStringEnclosedBy(line, INCLUDE_CHAPTER_START_DELIMITER, INCLUDE_CHAPTER_END_DELIMITER);
                    Chapter c = Chapter.constructChapter(chLocativeName);
                    chapters.add(c);
                }
            }

            reader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return chapters;
    }

    public static String getStringEnclosedBy(String line, String startDelimiter, String endDelimiter) {
        int start = line.indexOf(startDelimiter) + startDelimiter.length();
        int end = line.indexOf(endDelimiter, start);
        return line.substring(start, end);
    }
}